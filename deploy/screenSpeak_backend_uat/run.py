import time
import os
import subprocess
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

class JarFileHandler(FileSystemEventHandler):
    def __init__(self, filename, script):
        self.last_modified = 0
        self.filename = filename
        self.script = script

    def on_modified(self, event):
        if not event.is_directory and event.src_path.endswith(self.filename):
            # Check if the last modification was over 1 second ago
            if time.time() - self.last_modified > 1:
                print(f'{self.filename} has been modified. Executing {self.script}...')
                subprocess.call(['cmd', '/c', self.script])
                self.last_modified = time.time()

def monitor_file(filename, script):
    event_handler = JarFileHandler(filename, script)
    observer = Observer()
    observer.schedule(event_handler, path='.', recursive=False)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()

    observer.join()

if __name__ == "__main__":
    monitor_file('screenSpeak_backend-0.0.1-SNAPSHOT.jar', 'run.bat')