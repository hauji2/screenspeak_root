import time
import os
import subprocess
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

class JarFileHandler(FileSystemEventHandler):
    def __init__(self, filename, script):
        self.filename = filename
        self.script = script

    def on_modified(self, event):
        if not event.is_directory and event.src_path.endswith(self.filename):
            dir_path = os.path.dirname(event.src_path)  # Get the directory in which the file was changed
            script_path = os.path.join(dir_path, self.script)
            script_path = os.path.abspath(script_path)  # Convert script path to absolute path
            command = f"cmd /c {script_path}"
            print(f'{self.filename} has been modified. Executing {command}...')
            subprocess.call(command, shell=True)

def monitor_file(directories, filename, script):
    event_handler = JarFileHandler(filename, script)
    observers = []
    
    for directory in directories:
        observer = Observer()
        observer.schedule(event_handler, path=directory, recursive=False)
        observer.start()
        observers.append(observer)

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        for observer in observers:
            observer.stop()

    for observer in observers:
        observer.join()

if __name__ == "__main__":
    directories = ["./screenSpeak_backend_dev", "./screenSpeak_backend_uat", "./screenSpeak_backend_prod"]
    monitor_file(directories, 'screenSpeak_backend-0.0.1-SNAPSHOT.jar', 'run.bat')